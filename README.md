
# Flutter Course Projects

<p  align="center">
<a  href="https://docs.docker.com/"  target="blank"><img  src="https://storage.googleapis.com/cms-storage-bucket/6a07d8a62f4308d2b854.svg"  width="500"  alt="Flutter Logo"  /></a>
</p>
  

## Projects:

1. [Hello World Dart](https://gitlab.com/diegoauzad/flutter_course_projects/-/tree/main/01-dart-intro)

- Fundamentos del lenguaje de programación Dart.

2. [Counter App](https://gitlab.com/diegoauzad/flutter_course_projects/-/tree/main/hello_world_app)

- Manejo de estado básico en Dart y como actualizar el valor en pantalla.

3. [YesNo Chat App](https://gitlab.com/diegoauzad/flutter_course_projects/-/tree/main/yes_no_app)

- Creación de una aplicación que responde nuestras preguntas (siempre y cuando sean de si o no). Incluye un GIF en la respuesta.

4. [TokTik App](https://gitlab.com/diegoauzad/flutter_course_projects/-/tree/main/toktik)

- Clon de TikTok para mostrar videos a pantalla completa y sobreponiendo widgets.

5. [Widgets App](https://gitlab.com/diegoauzad/flutter_course_projects/-/tree/main/widgets_app)

- Aplicación con diferentes tipos de widgets y temas.

6. [Cinemapedia](https://gitlab.com/diegoauzad/flutter_course_projects/-/tree/main/cinemapedia)

- App en Flutter para mostrar una grilla de películas usando base de datos local y el API de TheMovieDB.

7. [Forms App](https://gitlab.com/diegoauzad/flutter_course_projects/-/tree/main/forms_app)

- Introducción básica a los gestores de estado Cubit y BloC ademas de ver como manejar campos de formulario de manera tradicional.

8. [Push & Local Notifications](https://gitlab.com/diegoauzad/flutter_course_projects/-/tree/main/push_app)

- Gestión de notificaciones push locales y remotas. Se crea y usa un proyecto Firebase para enviar notificaciones push remotas.

9. [Docker Teslo Backend](https://gitlab.com/diegoauzad/flutter_course_projects/-/tree/main/backend-teslo)

- Contenedor Docker para levantar el backend de la aplicación móvil del último proyecto (Teslo App).

10. [Teslo App](https://gitlab.com/diegoauzad/flutter_course_projects/-/tree/main/teslo_app)

- App de un catálogo de productos que se conecta al backend de la sección anterior. La app consume RESTful APIs para gestión de productos y también tiene autenticación con tokens JWT.
  
  

# Flutter Móvil - Instalaciones recomendadas

  

Descargar las hojas de atajos recomendadas:

  

[Guías de atajos - Dart ](https://devtalles.com/files/dart-cheat-sheet.pdf)

  

[Guías de atajos - Flutter ](https://devtalles.com/files/flutter-cheat-sheet.pdf)

  
  

# Instalaciones en el equipo

  

1. [Git](https://git-scm.com/)

```

git config --global user.name "Tu nombre"

git config --global user.email "Tu correo"

```

2. Crear [cuenta en GitHub](https://github.com/)

  

3. [VSCode - Visual Studio Code](https://code.visualstudio.com/)

  

4. [Postman](https://www.postman.com/downloads/)

  

5. [Android Studio](https://developer.android.com/studio)

  

6. Sólo Mac [xCode](https://apps.apple.com/ca/app/xcode/id497799835?mt=12)

  

7. [Flutter SDK](https://docs.flutter.dev/get-started/install)

  

8. [NodeJS](https://nodejs.org/en/)

  

9. [Docker Desktop](https://www.docker.com/)

  

10. [Table Plus](https://tableplus.com/) <-- Visor de base de datos

  
  

## Extensiones de VSCode

  

* [Activitus Bar](https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.activitusbar)

  

* [Error Lens](https://marketplace.visualstudio.com/items?itemName=usernamehw.errorlens)

  

* [Paste JSON as Code](https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype)

  

* [Better Comments](https://marketplace.visualstudio.com/items?itemName=aaron-bond.better-comments)

  

* [Terminal](https://marketplace.visualstudio.com/items?itemName=formulahendry.terminal)

  

* [Awesome Flutter Snippets](https://marketplace.visualstudio.com/items?itemName=Nash.awesome-flutter-snippets)

  

* [Bloc](https://marketplace.visualstudio.com/items?itemName=FelixAngelov.bloc)

  

* [Dart Language](https://marketplace.visualstudio.com/items?itemName=Dart-Code.dart-code)

  

* [Flutter Support](https://marketplace.visualstudio.com/items?itemName=Dart-Code.flutter)

  

* [Pubspec Assist](https://marketplace.visualstudio.com/items?itemName=jeroen-meijer.pubspec-assist)

  

* [Lorem ipsum](https://marketplace.visualstudio.com/items?itemName=Tyriar.lorem-ipsum)

  
  

### Tema en VSCode y configuraciones:

  

* [Aura Theme](https://marketplace.visualstudio.com/items?itemName=DaltonMenezes.aura-theme)

Configuraciones adicionales (opcionales) - settings.json

```

"workbench.colorCustomizations": {

"statusBar.background": "#121016",

"statusBar.debuggingBackground": "#121016",

"statusBar.debuggingForeground": "#525156",

"debugToolBar.background": "#121016",

},

"[dart]": {

"editor.formatOnSave": false,

"editor.formatOnType": true,

"editor.selectionHighlight": false,

"editor.suggest.snippetsPreventQuickSuggestions": false,

"editor.suggestSelection": "first",

"editor.tabCompletion": "onlySnippets",

"editor.wordBasedSuggestions": false

},

```

  

* [Iconos](https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme)

  

* [Otras hojas de atajos](https://cursos.devtalles.com/pages/mas-talento)