
# Forms

Se revisan los siguientes temas:

- **Manejo de formularios**
  - Tradicionales **Stateful** (Forms, TextFormField + Keys)
  - Con **gestor de estado**
  - Con **gestor de estado** + **Data Input fields** personalizados
  - Validaciones

- **Gestores de estado**
  - **BLoC** (_Business Logic Component_)
  - **Flutter Bloc**
  - **Cubits**
  - **Equatable**
  - **Eventos**
  - **Estado**


La idea de aprender un nuevo **gestor de estado**, es para obtener experiencia con diferentes gestores y lograr determinar cuál es el que mejor se adapta a nuestro estilo de programación, **Flutter Bloc** es bien robusto y a la vez puede verse como con mucho archivo adicional, pero a la larga, **lo hace fácil de probar, revertir y mantener**.


## Screenshots

![Simulator Screen Recording](ss/forms-app.gif)