
# Push App

Manejo de Notificaciones Push en Android / iOS.

**Push Notifications:**
- **Tipos** de estado de notificaciones
- **Métodos** para su manejo
- **Entidades**
- **BLoC**
- **Leer** las notificaciones push
- **Interacciones**
- **Navegación** a diferentes rutas basados en la **PUSH**
- **Firebase**
- Configuraciones de **FCM**
- Configuración de proyecto de **Firebase**

**Local Notifications:**
- Mezclar **Push** **+** **Local** Notifications
- **Reaccionar** cuando se tocan las local notifications
- Sonidos e iconos personalizados
- Aprender a **evitar** las dependencias ocultas

**iOS Notifications**
- Aprovisionamiento
- Credenciales y perfiles
- Dispositivo físico
- Conectar APNs (Apple push notification service) con FCM
- Recomendaciones y generalidades del proceso
- Interactuar con las notificaciones
- Imágenes en las notificaciones

## Screenshots

**Permission Request**
![Simulator Screenshot](ss/ss_0.png)

**Authorized**
![Simulator Screenshot](ss/ss_1.png)

