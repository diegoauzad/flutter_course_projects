
# Hello World App  (Counter App)

La primera aplicación de Flutter creada en el curso.

- Stateful y Stateless Widgets
- Scaffold
- FloatingActionButtons
- Column
- Widgets personalizados
- Constantes
- MaterialApp
- Introducción a Material Design 3
- Color Schemes
- AppBars
- y mucho más

También pasaremos por la explicación de cada archivo y directorio principal de un proyecto de Flutter, ahora sí, es momento de empezar a crear nuestras primeras aplicaciones móviles.

## Screenshots

![Simulator Screen Recording](ss/hello-world-app.gif)