# Teslo App

**Autenticación mediante JWT (Json Web Tokens)**
- Realizar el **POST** **HTTP**
- Obtener las **credenciales** del usuario
- Manejo de **errores** personalizados
- Manejo del estado del **formulario con Riverpod**
- Comunicación entre **providers**

A lo largo del proyecto se trabaja con:

- Cámara
- Tokens de acceso
- **CRUD** (Create Read Update Delete) Rest API Endpoints
- **Riverpod**
- **GoRouter**

Protección de rutas utilizando **Go_Router** + **Riverpod**:

- Proteger rutas
- Redireccionar
- Actualizar instancia del **GoRouter** cuando hay cambios en el estado
- Colocar **listeners** de GoRouter
- Change notifier
- **Preferencias** de usuario
- Almacenar **token** de acceso de forma **permanente**

Pantalla de **Productos** (Home):

- Login con el **boton done** del teclado
- **Masonry** ListView
- Productos
  - **Entidad**
  - **Datasources**
  - **Repositorios**
- Riverpod
  - **Provider**
  - **StateNotifierProvider**

**Creación y mantenimiento** de productos:
- Formularios
- Segmentos de botones
- **Selectores**
- **Posteos**
  - Path
  - Post
- Retroalimentación de **sucesos**
- Manejo de errores
- **Inputs** personalizados

**Cámara, Galeria y Carga de Archivos**
- **Patrón adaptador** sobre el paquete de cámara
- POST Form **Multipart**
- Mostrar imágenes como **archivos**
- Multiples cargas **simultáneas**
- Postman - Pruebas de API
- **Actualizar** **estado** del formulario
  

## Backend
- [Documentación de los endpoints disponibles](http://localhost:3000/api)
- [Teslo Backend](https://gitlab.com/diegoauzad/flutter_course_projects/tree/main/backend-teslo)

# Screenshots
![Simulator Screen Recording](ss/teslo-app.gif)