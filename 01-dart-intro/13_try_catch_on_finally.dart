void main() async {
  print('Program start');

  try {
    final value = await httpGet("https://www.google.com");
    print(value);
  } on Exception {
    print('Exception found');
  } catch (error) {
    print('Error: $error');
  }
  finally {
    print('Fin del try/catch');
  }

  print('Program end');
}

Future<String> httpGet(String url) async {
  await Future.delayed(const Duration(seconds: 1));

//   throw Exception('No parameters found');
  
  throw 'Unknown error';

//   return 'Response';
}
