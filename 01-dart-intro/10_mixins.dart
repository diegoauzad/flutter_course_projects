void main() {
  
  final flipper = Dolphin();
  flipper.swim();
  
  final batman = Bat();
  batman.fly();
  batman.walk();
  
  final namor = Duck();
  namor.fly();
  namor.walk();
  namor.swim();
}

abstract class Animal {
  
}

abstract class Mammphere extends Animal {
  
}

abstract class Bird extends Animal {
  
}

abstract class Fish extends Animal {
  
}

mixin Flying {
  void fly() => print('I am flying!');
}

mixin Walking {
  void walk() => print('I am walking!');
}

mixin Swimming {
  void swim() => print('I am swimming!');
}

class Dolphin extends Mammphere with Swimming {
  
}

class Bat extends Mammphere with Flying, Walking {
  
}

class Cat extends Mammphere with Flying, Walking {
  
}

class Dove extends Bird with Flying, Walking {
  
}

class Duck extends Bird with Flying, Walking, Swimming {
  
}

class Shark extends Fish with Swimming {
  
}

class FlyingFish extends Fish with Swimming, Flying {
  
}

