void main() async {
  print('Program start');

  try {
    final value = await httpGet("https://www.google.com");
    print(value);
  } catch (error) {
    print('Error: $error');
  }

  print('Program end');
}

Future<String> httpGet(String url) async {
  await Future.delayed(const Duration(seconds: 1));

  throw 'Error';

//   return 'Response';
}
