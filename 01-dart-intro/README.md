
# Dart Intro

Sección introductoria a los fundamentos de **Dart**.

### Temas:

- Hola Mundo
- Tipos de datos
- Estructuras de colección de datos
- Tipos de parámetros
- Clases
- Diferentes constructores
- Getters y Setters
- Extends, Implements y Mixins
- Futures
- Streams
- Async, Async* y Await
- Decoradores (@override)
- Y más

--- 
El objetivo es hacer una introduccion a **Dart** para faniliarizarse en el lenguaje, es muy similar a otros, y la curva de aprendizaje no es difícil, pero tengan presente que cada quien aprende a ritmos diferentes.