# Widgets App

Contiene lo fundamental para cualquier aplicación de Flutter como:

1.- **Navegación entre pantallas**

2.-  **Nuevos widgets**
- Botones y sus variantes
- Botones personalizados
- Tarjetas
- Tarjetas personalizadas
- Align

3.-  **Rutas**
- Propias de Flutter
- Go_Router
- Paths
- Configuraciones de router
- - Propio
- - De terceros

4.- **Más Widgets**
- RefreshIndicator
- InfiniteScroll
- ProgresIndicators
- - Lineales
- - Circulares
- - Controlados
- Animaciones
- Snackbars
- Diálogos
- Licencias
- Switches, Checkboxes, Radios
- Tiles
- Listas
- Pageviews

Esta sección servirá principalmente para dos cosas:

1. Aprender sobre "Drawers" menú laterales

2. Gestor de estado Riverpod

Cuando terminemos esta sección podremos comprender por qué Riverpod es tan popular, por qué es considerado el sucesor espiritual de Provider y también habremos usado varios Providers de Riverpod para gestionar nuestra aplicación.

Bonus:

También veremos un poco de cómo saber si el dispositivo tiene notch o un espacio no visible en pantalla y evitar colocar contenido ahí.

Se dejan las bases necesarias y así comprender mejor como construir nuestras aplicaciones.

# Screenshots

![Simulator Screen Recording](ss/widgets-app.gif)